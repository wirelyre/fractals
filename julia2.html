<!doctype html>
<title>Julia &mdash; WebGL2</title>
<style>
    body {
        margin: 0;
        overflow: hidden; /* no scrollbars */
    }
    canvas {
        /* 100% the size of the viewport */
        width: 100vw;
        height:100vh;
    }
    .error {
        margin: 15%;
        font-family: monospace;
        font-size: larger;
    }
</style>
<meta charset="utf-8"/>

<body>
<canvas id="plot"></canvas>

<!--
This is a WebGL visualization of Julia sets. WebGL is a way to access the GPU
from a web browser. WebGL is an API based on OpenGL.

Although you might think that GPUs and OpenGL are only useful for 3D
applications, they are in fact flexible enough to do any kind of graphics
processing (and more!). There is no magic 3D subsystem in your GPU. This
program does not use any 3D, nor anything resembling 3D.

After reading this code, you should be able to understand the basics of modern
computer graphics. (Although many details in newer APIs are different or
irrelevant, lots of core ideas are the same. GPUs have simply become more
general-purpose.)
-->

<!--
OpenGL/WebGL work using "shaders", programs that run in a rendering pipeline.

OpenGL shaders are written in a language called GLSL. It looks a lot like C.
We'll be seeing it shortly. During program initialization, the GLSL is compiled
to code that runs directly on the GPU.

In this file, shaders are declared in HTML directly using `script` tags.
Normally, `script` tags are only for code that runs in the browser. But since
the browser doesn't recognize the type (`x-shader`), it won't try to run it.
The code will remain as plain text, hidden on the page. We'll grab that plain
text during initialization.
-->

<!--
Many programs use exactly two shaders: a vertex shader and a fragment shader.

A vertex shader is run once per vertex. Its input is the vertex coordinates
(probably). It outputs a vertex, and optionally other data. In most 3D
programs, this is where you convert from world space (the virtual 3D world
inside your computer, represented as a quaternion) into screen space (pixel
positions). This computation is called "projection".

A fragment shader is run once per pixel (actually, once per fragment; fragments
usually correspond to pixels, though). Its input is anything calculated in
previous shaders.

In a simple 3D program, you might use a vertex shader to convert from world
space to screen space, then a fragment shader to apply a pre-rendered texture.
This program, however, has no world space and no pre-rendered textures.
-->

<!--
Our goal is to calculate the color of each pixel. In order to do that, we need:

- A coordinate system that goes from [-2, 2]
- Some code to run on each pixel given coordinates

The former will be provided by a vertex shader, and the latter by a fragment
shader.
-->

<!--
We want to cover the (rectangular) screen. We do this with two triangles. Our
vertex shader will thus execute six times per render.
-->
<script id="vshader" type="x-shader">#version 300 es
// This code is GLSL. It will be compiled.

in  vec4 position;       // Input to the shader.
out vec2 complex_coords; // Output from the shader.

void main() {
    // Since we aren't doing any 3D projection, we can just copy the vertex.
    gl_Position = position;
    // Now we figure out the complex coordinates of each vertex given its
    //   screen coordinates.
    // This is easy: in screen space, (-1, -1) is the lower left, and (1, 1)
    //   is the upper right.
    // We separate out X and Y, and scale by 2.
    complex_coords = vec2(position) * 2.0;
}
</script>

<!--
There are many implicit steps in the pipeline between the vertex and fragment
shader. Luckily, we don't care! All we know is that whenever our fragment
shader is called, it is given the values calculated by the vertex shader...

...except the fragment shader is called many more times than the vertex shader.
Which one provides the actual value? All of them: for each triange, the values
are interpolated between the values provided by each vertex of that triangle.
(By default this is done with respect to perspective. It's possible to use
other interpolation modes.)

Here's the data that's passed along the entire rendering pipeline:
   vertex coordinates {(-1, -1), (-1, 1), ...}
=> (vertex shader) => vertex coordinates + complex coordinates
=> (rasterizer) => HUGE LIST OF PIXELS + complex coordinates for each one
    ^~~~~~~~~~ this happens automatically
=> (fragment shader) => huge list of colors

And that's how we produce a picture from nothing. The only thing left is to
write a shader that takes complex coordinates and produces colors.
-->

<script id="fshader" type="x-shader">#version 300 es
precision highp float;

uniform vec2 c; // A uniform is a value that is constant throughout a render.
in  vec2 complex_coords; // Input from vertex shader.
out vec4 color;          // RGBA output.

vec4 colorize(int i) {
    return vec4(0, log(float(i)) / log(100.0), 0, 1);
}
vec2 julia(vec2 z, vec2 c) {
    return vec2(z.x*z.x - z.y*z.y, 2.0*z.x*z.y) + c;
}

void main() {
    vec2 z = complex_coords;
    // vec2 old = z; // for Mandelbrot set
    for (int i=0; i<100; i++) {
        if (z.x*z.x + z.y*z.y > 4.0) {
            color = colorize(i);
            return;
        }
        z = julia(z, c); // Julia set
        // z = julia(z, old); // Mandelbrot set
    }
    color = vec4(0, 0, 0, 1);
}
</script>

<!--
Remember: so far, all we have is plain text. Sure, it looks like a program, but
the browser can't execute it directly. The code below is what prepares the
browser, fetches the data, and links it all together.

To produce this code, I read two WebGL tutorials simultaneously, then gutted
everything until I liked it. Now, some of the error handling is gone, but it's
still definitely hackable. It will display an error if:
- Your browser doesn't support WebGL2; or
- There's an error in the shader code.

Frankly, even though it took the longest to write, this part is pretty boring.
It's well worth reading and understanding, but it's way more fun to fiddle with
the fragment shader.
-->

<script>
    "use strict";

    let canvas = document.getElementById('plot');

    // Display an error, replacing the canvas with the provided text.
    let error = (text) => {
        let elem = document.createElement('div');
        elem.appendChild(document.createTextNode(text));
        elem.classList.add('error');
        canvas.parentElement.replaceChild(elem, canvas);
        throw new Error(text);
    };

    let gl = canvas.getContext('webgl2');
    if (!gl) { error('😞 Browser does not appear to support WebGL2.'); }

    // Compile a shader given the id of an HTML element containing its source.
    let loadShader = (ctx, id, type) => {
        let src = document.getElementById(id);
        let shader = ctx.createShader(type);
        ctx.shaderSource(shader, src.text);
        ctx.compileShader(shader);

        if (!ctx.getShaderParameter(shader, ctx.COMPILE_STATUS)) {
            let errorLog = ctx.getShaderInfoLog(shader);
            ctx.deleteShader(shader);
            error('Error in shader "'+id+'": '+errorLog);
            return;
        }

        return shader;
    };

/* Here's how a typical OpenGL/WebGL programs initializes:
   1. Compile some shaders.
   2. Form (possibly several) programs out of sequences of those shaders.

   When rendering:
   1. Say "GL, please clear the screen".
   2. Say "GL, please use _this_ program until otherwise instructed".
   3. Say "GL, render _this_ array using the current program".
   4. Repeat steps 2 and 3 until the scene is complete.

   This program doesn't clear the screen because it always draws over the
   entire viewport.
*/

    let program = gl.createProgram();
    // Compile the shaders.
    gl.attachShader(program, loadShader(gl, 'vshader', gl.VERTEX_SHADER));
    gl.attachShader(program, loadShader(gl, 'fshader', gl.FRAGMENT_SHADER));
    // Provide a way (id 0) to refer to the `position` vertex attribute.
    gl.bindAttribLocation(program, 0, 'position');
    // Finish initialization.
    gl.linkProgram(program);
    gl.useProgram(program);

    // Load vertex data to cover the viewport.
    let positionBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
    // It is possible to avoid repeating vertices, but it makes this program
    // much longer.
    gl.bufferData(gl.ARRAY_BUFFER,
        new Float32Array([-1, -1,   1, 1,   -1, 1,
                          -1, -1,   1, 1,   1, -1]),
        gl.STATIC_DRAW);
    //                         ┌─the `position` vertex attribute
    gl.enableVertexAttribArray(0);
    gl.vertexAttribPointer(0, 2, gl.FLOAT, false, 0, 0);

    // Note the implicit state:
    // - `program` is loaded as the shading pipeline.
    // - `positionBuffer` is loaded as the array provided to the pipeline.
    // Typical programs set this state every frame, but since we only have one
    // of each, we never swap them out.

    let draw = () => {
        // We don't ever have to clear the buffer because we cover the entire
        // viewport with every render.
        /*
        gl.clearColor(0, 0, 0, 1);
        gl.clear(gl.COLOR_BUFFER_BIT);
        */

        //           starting index─┐  ┌─number of indices
        gl.drawArrays(gl.TRIANGLES, 0, 6);
    }

    // Sizes the viewport from the actual canvas dimensions, then draws.
    let reshape = () => {
        canvas.width = canvas.clientWidth;
        canvas.height = canvas.clientHeight;
        gl.viewport(0, 0, canvas.clientWidth, canvas.clientHeight);
        draw();
    };

    window.addEventListener('resize', reshape);

    canvas.addEventListener('mousemove', (event) => {
        // Set `c` to where the mouse is in complex coordinates.
        let rect = canvas.getBoundingClientRect();
        gl.uniform2f(gl.getUniformLocation(program, 'c'),
            (event.clientX - rect.left) / canvas.width * 4 - 2,
            (event.clientY - rect.top) / canvas.height * 4 - 2);

        draw();
    });

    // Set the initial value for `c`.
    gl.uniform2f(gl.getUniformLocation(program, 'c'), 0.3, 0.6);

    // Perform an initial render.
    reshape();
</script>
