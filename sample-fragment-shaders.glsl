/* Alternate colorization */

/* Shades of green for integer iteration count */
vec4 colorize(int i) {
    return vec4(0, log(float(i)) / log(100.0), 0, 1);
}

/* Shades of color (red to violet) for integer iteration count */
vec4 colorize(int i) {
    return vec4(hsv2rgb(vec3(float(i)/100.0, 0.9, 0.9)), 1);
}

/* Shades of color for fractional iteration count */
vec4 colorize(float c) {
    return vec4(hsv2rgb(vec3(0.95+c/2.0, 0.9, 0.9)), 1);
}

/* Shades of progressively devalued color for fractional iteration count */
vec4 colorize(float s) {
    return vec4(hsv2rgb(vec3(s/5.0, 0.9, exp(-s/30.0))), 1);
}


/* Julia set renderings */

/* Keeping track of integer number of iterations */
void main() {
    vec2 z = coords * 2.0;
    for (int i=0; i<100; i++) {
        if (z.x*z.x + z.y*z.y > 4.0) {
            color = colorize(i);
            return;
        }
        z = julia(z, mouse * 2.0);
    }
    color = vec4(0, 0, 0, 1);
}

/* Smoothly keeping track of iterations */
/* See http://stackoverflow.com/a/1243788 */
void main() {
    vec2 z = coords * 2.0;
    float s = exp(-length(z));
    for (int i=0; i<100; i++) {
        if (z.x*z.x + z.y*z.y > 4.0) {
            color = colorize(s);
            return;
        }
        z = julia(z, mouse * 2.0);
        s += exp(-length(z));
    }
    color = vec4(0, 0, 0, 1);
}
