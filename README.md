# fractals

Several JavaScript fractal renderers that run in a web browser.

## `julia1.html`

This page uses pure JavaScript to render Julia sets on an HTML canvas. The code
iterates through each pixel, determining its color. Finally, the canvas updates
with the pixel data.

Mousing over the canvas changes the Julia parameter to the mouse's position.
Toggling the checkbox changes the number of iterations per pixel.

This file's documentation explains some ES6 syntax. It should be legible to
someone who can read a C-family language.

## `julia2.html`

This page uses WebGL to render Julia sets. The code sets up a WebGL context,
then requests a render. The render probably occurs on the GPU.

Mousing over the canvas changes the Julia parameter to the mouse's position. The
code converts the mouse's position in screen pixels to the plot's coordinate
system, updates the uniform (variable) through WebGL, then requests a rerender.

This file's documentation provides a detailed introduction to OpenGL and WebGL.
It should be useful for someone who generally understands computer graphics, but
knows very little about OpenGL and graphics pipelines.

## `fractals.html`

This page provides a framework for exploring graphics programming. It provides
an editor for a fragment shader and several useful uniforms.

The fragment shader source is editable in the embedded
[Ace editor](https://ace.c9.io/).  When the source changes, the "Compile" button
turns yellow. Pressing the "Compile" button attempts to compile and run the
fragment shader. If there are errors, the button turns red and the errors are
displayed. Otherwise, the button turns green and the new program runs.

The table between the editor and the buttons displays the uniforms provided to
the program:

- `time` is the number of milliseconds since the program began.
- `mouse` is the coordinates of the last mouseover.
- `clicked` is the coordinates of the last mouse click.

Pressing the "Reset" button restarts the program and sets `time` to 0.

If the checkbox next to a uniform is unchecked, the uniform's value will not
update when receiving events (frame advances, mouseovers, or mouse clicks).

All coordinates (`coords`, `mouse`, and `clicked`) range from (-1, -1) in the
bottom-left corner to (1, 1) in the upper-right corner.

The code in this file is not documented.
